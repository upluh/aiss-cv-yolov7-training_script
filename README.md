# About this repository
This repo explains how the YOLOv7 model is trained on the bwUniCluster2.0 via their Jupyter services.
For documentation on how to get access please refer to the [Jupyter Documentation](https://wiki.bwhpc.de/e/BwUniCluster2.0/Jupyter). Once you have gained access to the hpc (and have initiated a session) you have to follow the training instructions outlined in our [Jupyter Notebook](YOLOv7tiny.ipynb) .
To find the ouputs of our runs please refer to [yolo_models: yolov7](https://git.scc.kit.edu/aiss_cv/yolo_models/yolov7)
